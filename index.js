const express = require('express');
const cors = require('cors')
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const yaml = require('js-yaml');
var fs = require("fs");
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var championRouter = require('./routers/championRouter.js');
var itemRouter = require('./routers/itemRouter.js');
var gameRouter = require('./routers/gameRouter.js');
var roundRouter = require('./routers/roundRouter.js');
var testRouter = require('./routers/testRouter.js');
var outputRouter = require('./routers/outputRouter.js');
var statsRouter = require('./routers/statsRouter.js');


//var Routes = require("./routes/routes.js");

var app = express();

//create swagger configuration
var file = fs.readFileSync("openapi.yaml")
var data = yaml.safeLoad(file);
const swaggerOptions = data;

//register swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerOptions));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

////Create connection to database
//Local Connection
// var mongo = 'mongodb://testuser:testpass@localhost:27017/tfttest';

//RancherOS Connection
var mongo = 'mongodb://backend:Startthegame@ec2-13-57-233-87.us-west-1.compute.amazonaws.com:27017/Set4'
mongoose.connect(mongo, { useNewUrlParser: true, useUnifiedTopology: true });

//Routes
/**
 * @swagger
 * /swagger:
 *  get:
 *    description: this is s atest
 *    responses:
 *      '201':
 *        description: this is a test
 */
app.get('/swagger', (req, res) => {
  res.send('Hello Kevin!')
});

app.use('/champion', championRouter);
app.use('/item', itemRouter);
app.use('/game', gameRouter);
app.use('/round', roundRouter);
app.use('/test', testRouter);
app.use('/output', outputRouter);
app.use('/stats', statsRouter);



var port = 8000;
app.listen(port, () => {
  console.log('Example app listening on port ' + port + '!');
});
