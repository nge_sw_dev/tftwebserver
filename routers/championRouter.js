var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var ChampionLookup = require('../models/championLookupSchema.js');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');



/*router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
*/

//Retreive list of champions
router.get('/lookup', function (req, res) {
  var query = ChampionLookup.find();
  query.then(function (doc) {
    res.send(doc);
  });
});



//Create new entry in champion lookup table; passes name in address and fields in body
router.post('/lookup', function (req, res) {
  var championLookup = new ChampionLookup({
    name: req.body.name,
    origin: req.body.origin,
    class: req.body.class,
    cost: req.body.cost,
    originWeight: req.body.originWeight,
    classWeight: req.body.classWeight
  });

  championLookup.save(function (err) {
    if (err) throw err;
  });
  res.send(championLookup);
});



module.exports = router;
