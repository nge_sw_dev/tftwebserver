var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Game = require('../models/gameSchema.js');
var Round = require('../models/roundSchema.js');
var BoardState = require('../models/boardStateSchema.js');
var ChampionLookup = require('../models/championLookupSchema.js');
var ItemLookup = require('../models/itemLookupSchema.js');
const { writeLog } = require('../js/logHelper');

//get most recently created round
router.get('/current', function (req, res) {
  var query = Game.findOne().sort({ date: -1 }).limit(1)
    .populate({
      path: 'rounds',
      model: 'Round',
      populate: {
        path: 'boardState',
        model: 'BoardState'
      }
    });
  query.then(function (game) {
    res.send(game.rounds[game.currentRound - 1]);
  });
});

//get the most recently SUBMITTED round
router.get('/current/submitted', function (req, res) {
  var query = Game.findOne().sort({ date: -1 }).limit(1);
  query.then(function (game) {
    var roundQuery = Round.findOne({ dayNumber: game.dayNumber, gameNumber: game.gameNumber, complete: true }).sort({ date: -1 }).limit(1).populate('boardState');
    roundQuery.then(function (round) {
      res.send(round);
    });
  });
});


router.get('/specific/:day/:game:/:round', function (req, res) {
  var query = BoardState.find({ dayNumber: req.params.day, gameNumber: req.params.game, roundNumber: req.params.round });
  query.then(function (doc) {
    res.send(doc);
  });
});


//submit board state from player, will wait for w/l info to move round forward.
router.post('/', function (req, res) {
  var query = Game.findOne().sort({ date: -1 }).limit(1);
  query.then(function (game) {
    var boardState = new BoardState({
      name: req.body.name,
      spotter: req.body.spotter,
      dayNumber: game.dayNumber,
      gameNumber: game.gameNumber,
      champions: req.body.champions,
      items: req.body.items,
      health: 100,
      submitted: false
    });

    //NOT CORRECT PVE CALCULATION
    if (boardState.roundNumber <= 3 || ((boardState.roundNumber - 3) % 5 == 0 && boardState.roundNumber < 34)) {
      boardState.pve = true;
    } else {
      boardState.pve = false;
    }
    //from here up it works
    var champNames = [];
    var itemNames = [];
    req.body.champions.forEach(function (champ) {
      champNames.push(champ.name);
      champ.items.forEach(function (item) {
        if (item != "") {
          itemNames.push(item);
        }
      });
    });
    req.body.items.forEach(function (item) {
      if (item != "") {
        itemNames.push(item);
      }
    });
    boardState.championNames = champNames;
    boardState.itemNames = itemNames;
    var championQuery = ChampionLookup.find({ name: boardState.championNames }).lean();
    var itemQuery = ItemLookup.find({ name: boardState.itemNames }).sort({ itemPower: 'descending' }).lean();
    championQuery.then(function (championDoc) {
      itemQuery.then(function (itemDoc) {
        boardState.championDoc = championDoc;
        boardState.itemDoc = itemDoc;
        var sortedItems = [];
        var uncombinedItems = [];
        itemDoc.forEach(function (doc) {
          itemNames.forEach(function (name) {
            if (doc.name == name) {
              itemNames.push(name);
            }
          });
          if (!doc.combined) {
            uncombinedItems.push(doc.name);
          }
        });
        while (uncombinedItems.length < 12) {
          uncombinedItems.push("");
        }
        boardState.sortedItems = sortedItems;
        boardState.uncombinedItems = uncombinedItems;
        boardState.calculateChampionPower().then(function (power) {
          boardState.calculateCarry().then(function (carry) {
            boardState.calculateItemPower().then(function (itemPower) {
              boardState.calculateSynergies().then(function (synergies) {
                boardState.top3ChampionCalculate().then(function (top3Champs) {

                  var roundQuery = Round.find({ dayNumber: game.dayNumber, gameNumber: game.gameNumber, complete: false }).sort({ date: -1 }).populate('boardState');
                  roundQuery.then(function (doc) {
                    var check = false;
                    var round = false;
                    doc.forEach(function (currentRound) {
                      check = currentRound;
                      check.boardState.forEach(function (state) {
                        if (state.name == boardState.name) {
                          check = false;
                        }
                      });
                      if (check != false) {
                        round = check;
                      }
                    });
                    //IF NEW ROUND MUST BE CRATED
                    if (round == false) {
                      game.currentRound++;
                      console.log("Round " + game.currentRound + " created.");
                      writeLog("Round " + game.currentRound + " created.");
                      var round = new Round({
                        dayNumber: game.dayNumber,
                        gameNumber: game.gameNumber,
                        roundNumber: game.currentRound,
                        date: Date.now(),
                        boardState: [],
                        complete: false
                      });
                      if (round.roundNumber <= 3 || ((round.roundNumber - 3) % 5 == 0 && round.roundNumber < 34)) {
                        round.pve = true;
                      } else {
                        round.pve = false;
                      }
                      game.rounds.push(round._id);
                      round.boardState.push(boardState._id);
                      boardState.roundNumber = round.roundNumber;
                      boardState.championPower = power;
                      boardState.carryChampion = carry;
                      boardState.itemPower = itemPower;
                      boardState.synergies = synergies.synergies;
                      boardState.top3Champs = top3Champs;
                      boardState.activeSynergies = synergies.activeSynergies;
                      boardState.save(function (err) {
                        if (err) throw err;
                      });
                      game.save(function (err) {
                        if (err) throw err;
                      });
                      round.save(function (err) {
                        if (err) throw err;
                      });
                      console.log("Boardstate sent for " + req.body.spotter + " on round " + boardState.roundNumber);
                      writeLog("Boardstate sent for " + req.body.spotter + " on round " + boardState.roundNumber)
                      res.send(round);
                    } else {
                      //NEW ROUND DOESN'T NEED TO BE CREATED
                      round.boardState.push(boardState._id);
                      boardState.roundNumber = round.roundNumber;
                      boardState.championPower = power;
                      boardState.carryChampion = carry;
                      boardState.itemPower = itemPower;
                      boardState.synergies = synergies.synergies;
                      boardState.top3Champs = top3Champs;
                      boardState.activeSynergies = synergies.activeSynergies;
                      boardState.save(function (err) {
                        if (err) throw err;
                      });
                      game.save(function (err) {
                        if (err) throw err;
                      });
                      round.save(function (err) {
                        if (err) throw err;
                      });
                      console.log("Boardstate sent for " + req.body.spotter + " on round " + boardState.roundNumber);
                      writeLog("Boardstate sent for " + req.body.spotter + " on round " + boardState.roundNumber)
                      res.send(round);
                    }
                  });
                });
              });
            });
          });
        });
      });
    });
  });
});




//post round with a specified round Number
router.post('/update/:roundNumber', function (req, res) {
  var query = Game.findOne().sort({ date: -1 }).limit(1);
  query.then(function (game) {
    var boardState = new BoardState({
      name: req.body.name,
      dayNumber: game.dayNumber,
      gameNumber: game.gameNumber,
      roundNumber: req.params.roundNumber,
      champions: req.body.champions,
      items: req.body.items,
      submitted: false
    });
    if (!req.body.hasOwnProperty("champions")) {
      boardState.champions = [];
    }
    if (!req.body.hasOwnProperty("items")) {
      boardState.items = [];
    }
    if (boardState.roundNumber <= 3 || ((boardState.roundNumber - 3) % 5 == 0 && boardState.roundNumber < 34)) {
      boardState.pve = true;
    } else {
      boardState.pve = false;
    }
    var champNames = [];
    var itemNames = [];
    req.body.champions.forEach(function (champ) {
      champNames.push(champ.name);
      champ.items.forEach(function (item) {
        if (item != "") {
          itemNames.push(item);
        }
      });
    });
    req.body.items.forEach(function (item) {
      if (item != "") {
        itemNames.push(item);
      }
    });
    boardState.championNames = champNames;
    boardState.itemNames = itemNames;
    var championQuery = ChampionLookup.find({ name: boardState.championNames }).lean();
    var itemQuery = ItemLookup.find({ name: boardState.itemNames }).sort({ itemPower: 'descending' }).lean();
    championQuery.then(function (championDoc) {
      itemQuery.then(function (itemDoc) {
        boardState.championDoc = championDoc;
        boardState.itemDoc = itemDoc;
        var sortedItems = [];
        var uncombinedItems = [];
        itemDoc.forEach(function (doc) {
          itemNames.forEach(function (name) {
            if (doc.name == name) {
              itemNames.push(name);
            }
          });
          if (!doc.combined) {
            uncombinedItems.push(doc.name);
          }
        });
        boardState.sortedItems = sortedItems;
        boardState.uncombinedItems = uncombinedItems;
        while (uncombinedItems.length < 12) {
          uncombinedItems.push("");
        }
        boardState.calculateChampionPower().then(function (power) {
          boardState.calculateCarry().then(function (carry) {
            boardState.calculateItemPower().then(function (itemPower) {
              boardState.calculateSynergies().then(function (synergies) {
                boardState.top3ChampionCalculate().then(function (top3Champs) {
                  boardState.top3Champs = top3Champs;
                  boardState.championPower = power;
                  boardState.carryChampion = carry;
                  boardState.itemPower = itemPower;
                  boardState.synergies = synergies.synergies;
                  boardState.activeSynergies = synergies.activeSynergies;
                  var updateQuery = BoardState.updateOne({ name: boardState.name, dayNumber: boardState.dayNumber, gameNumber: boardState.gameNumber, roundNumber: boardState.roundNumber }, { champions: boardState.champions, items: boardState.items, pve: boardState.pve, synergies: boardState.synergies, activeSynergies: boardState.activeSynergies, championPower: boardState.championPower, itemPower: boardState.itemPower, carryChampion: boardState.carryChampion, championNames: boardState.championNames, itemNames: boardState.itemNames, championDoc: boardState.championDoc, itemDoc: boardState.itemDoc, top3Champs: boardState.top3Champs });
                  updateQuery.then(function (doc) {
                    if (doc.hasOwnProperty('upserted')) {
                      var roundQuery = Round.findById(game.rounds[game.currentRound - 1]);
                      roundQuery.then(function (round) {
                        round.boardState.push(doc.upserted._id);
                        round.save(function (err) {
                          if (err) throw err;
                        });
                        console.log("Inserting boardstate for " + req.body.spotter);
                        writeLog("Inserting boardstate for " + req.body.spotter)
                      });
                    } else {
                      console.log("Updating boardstate for " + req.body.spotter);
                      writeLog("Updating boardstate for " + req.body.spotter)
                      res.send(doc);
                    }
                  });
                });
              });
            });
          });
        });
      });
    });
  });
});

//send w/l and health info; handles moving the round forward or not
router.post('/submit', function (req, res) {
  var query = Game.findOne().sort({ date: -1 }).limit(1);
  query.then(function (game) {
    var roundQuery = Round.find({ dayNumber: game.dayNumber, gameNumber: game.gameNumber, complete: false }).sort({ date: 1 }).populate('boardState');
    roundQuery.then(function (doc) {
      var check = false;
      var round = false;
      doc.forEach(function (currentRound) {
        check = currentRound;
        check.boardState.forEach(function (state) {
          if (state.name == req.body.name && state.submitted == false && round == false) {
            round = check;
          }
        });
      });
      var count = 0;

      //TODO: SOMETIMES ROUND IS STILL FALSE, IF A PLAYER IS THE FIRST TO SUBMIT TO A ROUND BUT PREVIOUSLY RESUBMIT THEIR BOARDSTATE THIS WILL HAPPEN
      round.boardState.forEach(function (state) {
        if (state.name == req.body.name) {
          state.win = req.body.win;
          state.health = req.body.health;
          state.opponent = req.body.opponent;
          state.submitted = true;
          state.save(function (err) {
            if (err) throw err;
          });

          if (state.health <= 0) {
            game.standings.unshift(state.name);
          }
        }
        if (state.submitted) {
          count++;
        }
      });
      console.log(count);
      if (count >= game.live) {
        var liveCount = 0;
        round.boardState.forEach(function (state) {
          if (state.health > 0) liveCount++;
        });
        console.log("liveCount for round " + round.roundNumber + ": " + liveCount);
        writeLog("liveCount for round " + round.roundNumber + ": " + liveCount)
        game.live = liveCount;
        if (liveCount > 1) {
          round.complete = true;
          round.save(function (err) {
            if (err) throw err;
          });
          game.save(function (err) {
            if (err) throw err;
          });
          console.log("Submitting health for " + req.body.spotter + " on round " + round.roundNumber);
          writeLog("Submitting health for " + req.body.spotter + " on round " + round.roundNumber);
          res.send(round);
        } else {
          game.complete = true;
          game.rounds.pop();
          game.currentRound--;
          game.calculateWinner(game).then(function (winner) {
            game.calculatePlayers(game).then(function (names) {
              game.standings.unshift(winner.name);
              game.winner = winner;
              game.players = names;
              var newGame = new Game({
                dayNumber: game.dayNumber,
                gameNumber: game.gameNumber + 1,
                currentRound: 1,
                date: Date.now(),
                players: game.players,
                complete: false,
                winner: {},
                live: 8,
                rounds: []
              });
              var nextGameRound = new Round({
                dayNumber: newGame.dayNumber,
                gameNumber: newGame.gameNumber,
                roundNumber: 1,
                pve: true,
                date: Date.now(),
                boardState: [],
                complete: false
              });
              nextGameRound.save(function (err) {
                newGame.rounds.push(nextGameRound._id);
                newGame.save(function (err) {
                  if (err) throw err;
                });
              });
              game.save(function (err) {
                if (err) throw err;
              });
              round.complete = true;
              round.save(function (err) {
                if (err) throw err;
              });
              console.log('sending winner');
              writeLog('sending winner');
              console.log("Submitting health for " + req.body.spotter + " on round " + round.roundNumber);
              writeLog("Submitting health for " + req.body.spotter + " on round " + round.roundNumber);
              res.send({ 'winner': game.winner });
            });
          });
        }
      } else {
        game.save(function (err) {
          if (err) throw err;
        });
        round.save(function (err) {
          if (err) throw err;
        });
        console.log("Submitting health for " + req.body.spotter + " on round " + round.roundNumber);
        writeLog("Submitting health for " + req.body.spotter + " on round " + round.roundNumber);
        res.send(round);
      }
    });
  });
});

//Recall method to get the most recent board state of the player with the given name
router.get('/recall', function (req, res) {
  var query = BoardState.findOne({ name: req.body.name }).sort({ date: -1 }).limit(1);
  query.then(function (state) {
    res.send(state);
  });
});




module.exports = router;
