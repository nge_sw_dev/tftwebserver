var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var ItemLookup = require('../models/itemLookupSchema.js');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');



/*router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
*/

//Retreive list of champions
router.get('/lookup', function (req, res) {
  var query = ItemLookup.find();
  query.then(function (doc) {
    var names = [];
    doc.forEach(function (item) {
      names.push(item.name);
    });
    res.send(names);
  });
});



//Create new entry in champion lookup table; passes name in address and fields in body
router.post('/lookup', function (req, res) {
  var item = new ItemLookup({
    name: req.body.name,
    combined: req.body.combined,
    recipe: req.body.recipe,
    itemPower: req.body.itemPower,
    origin: req.body.origin,
    class: req.body.class
  });
  item.save(function (err) {
    if (err) throw err;
  });
  res.send(item)
});



module.exports = router;
