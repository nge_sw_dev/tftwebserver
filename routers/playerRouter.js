var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Player = require('../models/playerModel.js');

/*
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
*/


//Gets list of all current players
router.get('/', function (req, res) {
  var query = Player.find()
  .populate('board')
  .populate('poolItems')
  .populate('equippedItems');
  query.then(function (players){
    res.send(players);
  });
})

//Get :name player
router.get('/:name', function (req, res) {
  var query = Player.findOne({username: req.params.name})
  .populate('board')
  .populate('poolItems')
  .populate('equippedItems');
  query.then(function (player){
    res.send(player);
  });
});

router.get('/:name/update', function(req,res) {
  var query = Player.findOne({name: req.params.name});
  query.then(function(player) {
    console.log(player);
    player.getGames(player.name).then(function(games) {
      console.log(games);
      player.games = games;
      player.getFinalBoardStates(player).then(function(states) {
        player.finalStates = states;
        player.save(function(err) {
          if(err) throw err;
        });
        res.send(player);
      })
    });
  });
});

//Creates a user
router.post('/:name/create', function (req, res) {
  var player = new Player({
    name: req.params.name,
    games: [],
    finalStates: []
  });
  player.save(function(err) {
    if(err) throw err;
  });
  res.send(player);
});


module.exports = router;
