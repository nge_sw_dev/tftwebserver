var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var BoardState = require('../models/boardStateSchema');
var ItemLookup = require('../models/itemLookupSchema.js');
var ChampionLookup = require('../models/championLookupSchema.js');
var Game = require('../models/gameSchema.js');
const { getLog } = require('../js/logHelper');

router.post('/', function (req, res) {
  var boardState = BoardState({
    champions: req.body.champions,
    items: req.body.items
  });
  boardState.calculateChampionPower().then(function (power) {
    res.send({ 'power': power });
  });
});

router.get('/', function (req, res) {
  var query = Game.findOne({ dayNumber: 1, gameNumber: 1 }).limit(1);
  query.then(function (game) {
    game.calculateWinner().then(function (winner) {
      game.winner = winner;
      game.save(function (err) {
        if (err) throw err;
      });
      res.send(game);
    });
  });
});

router.get('/log/:n', async function (req, res) {
  try {

    var logs = await getLog(req.params.n);
    res.send(logs);
  } catch (e) {
    console.log('error found');
    res.send([]);
  }
})


router.get('/champion', function (req, res) {
  var query = ChampionLookup.find();
  query.then(function (doc) {
    var names = [];
    doc.forEach(function (item) {
      names.push(item.name);
    });
    res.send(names);
  });
});

router.get('/fix', function (req, res) {
  var gameQuery = Game.findOne({ dayNumber: 1, gameNumber: 6 });
  gameQuery.then(function (game) {
    var query = BoardState.findOne({ dayNumber: 1, gameNumber: 7, roundNumber: 1, name: "holythoth" });
    query.then(function (state) {
      game.winner = state;
      game.save(function (err) {
        if (err) throw err;
      });
      res.send(game);
    });
  });
});

router.post('/recalculatestate', function (req, res) {
  console.log(req.body.gameNumber);
  var query = BoardState.findOne({ dayNumber: 1, gameNumber: req.body.gameNumber, roundNumber: req.body.roundNumber, name: req.body.name });
  query.then(function (boardState) {
    console.log(boardState);
    var champNames = [];
    var itemNames = [];
    boardState.champions.forEach(function (champ) {
      champNames.push(champ.name);
      champ.items.forEach(function (item) {
        if (item != "") {
          itemNames.push(item);
        }
      });
    });
    boardState.items.forEach(function (item) {
      if (item != "") {
        itemNames.push(item);
      }
    });
    boardState.championNames = champNames;
    boardState.itemNames = itemNames;
    var championQuery = ChampionLookup.find({ name: boardState.championNames }).lean();
    var itemQuery = ItemLookup.find({ name: boardState.itemNames }).sort({ itemPower: 'descending' }).lean();
    championQuery.then(function (championDoc) {
      itemQuery.then(function (itemDoc) {
        boardState.championDoc = championDoc;
        boardState.itemDoc = itemDoc;
        var sortedItems = [];
        var uncombinedItems = [];
        itemDoc.forEach(function (doc) {
          itemNames.forEach(function (name) {
            if (doc.name == name) {
              itemNames.push(name);
            }
          });
          if (!doc.combined) {
            uncombinedItems.push(doc.name);
          }
        });
        while (uncombinedItems.length < 12) {
          uncombinedItems.push("");
        }
        boardState.sortedItems = sortedItems;
        boardState.uncombinedItems = uncombinedItems;
        boardState.calculateChampionPower().then(function (power) {
          boardState.calculateCarry().then(function (carry) {
            boardState.calculateItemPower().then(function (itemPower) {
              boardState.calculateSynergies().then(function (synergies) {
                boardState.championPower = power;
                boardState.carryChampion = carry;
                boardState.itemPower = itemPower;
                boardState.synergies = synergies.synergies;
                boardState.activeSynergies = synergies.activeSynergies;
                boardState.save(function (err) {
                  if (err) throw err;
                });
                res.send(boardState);
              });
            });
          });
        });
      });
    });
  });
});

module.exports = router;
