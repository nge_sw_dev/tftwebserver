var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Game = require('../models/gameSchema.js');
var Round = require('../models/roundSchema.js');
var BoardState = require('../models/boardStateSchema.js');
var ChampionLookup = require('../models/championLookupSchema.js');
var ItemLookup = require('../models/itemLookupSchema.js');


// Retrieves an object with the number of wins losses and draws for a given player name during a given game
router.get('/wl/:name/:day/:game', function (req, res) {
  var result = {
    win: 0,
    draw: 0,
    loss: 0
  };
  var query = BoardState.find({ name: req.params.name, dayNumber: req.params.day, gameNumber: req.params.game, submitted: true }).lean();
  query.then(function (doc) {
    doc.forEach(function (state) {
      switch (state.win + 1) {
        case 0:
          result.loss++;
          break;
        case 1:
          result.draw++;
          break;
        case 2:
          result.win++;
          break;
      }
    });
    res.send({ "result": result });
  });
});


// Retrieves an object with the number of wins losses and draws for a given player name during a given day
router.get('/wl/:name/:day', function (req, res) {
  var result = {
    win: 0,
    draw: 0,
    loss: 0
  };
  var query = BoardState.find({ name: req.params.name, dayNumber: req.params.day, submitted: true }).lean();
  query.then(function (doc) {
    doc.forEach(function (state) {
      switch (state.win + 1) {
        case 0:
          result.loss++;
          break;
        case 1:
          result.draw++;
          break;
        case 2:
          result.win++;
          break;
      }
    });
    res.send({ "result": result });
  });
});

//retrieves the round information for the round that a give player lost the most health in a given game
router.get('/largestloss/:name/:day/:game', function (req, res) {
  var query = BoardState.find({ name: req.params.name, dayNumber: req.params.day, gameNumber: req.params.game, submitted: true }).sort({ roundNumber: 'ascending' }).lean();
  var result = {
    loss: 0,
    round1: {},
    round2: {}
  };
  query.then(function (doc) {
    for (var i = 1; i < doc.length; i++) {
      if (doc[i - 1].health - doc[i].health > result.loss) {
        result.loss = doc[i - 1].health - doc[i].health;
        result.round1 = doc[i - 1];
        result.round2 = doc[i];
      }
    }
    res.send(result);
  });
});

//retrieves the board state of the winner from the last round in a specified game
router.get('/winner/:day/:game', function (req, res) {
  var query = Game.findOne({ dayNumber: req.params.day, gameNumber: req.params.game, complete: true }).lean();
  query.then(function (game) {
    if (game !== null && 'winner' in game) {
      res.send({ 'winner': game.winner });
    } else {
      res.send('no winner right now');
    }
  });
});


router.get('/winstreak:day/:game', function (req, res) {
  var query = BoardState.find({ name: req.body.name, dayNumber: req.params.day, gameNumber: req.params.game, pve: false }).sort({ roundNumber: 'ascending' }).lean();
  var streak = 0;
  var current = 0;
  query.then(function (doc) {
    doc.forEach(function (state) {
      if (state.win == 1) {
        current++;
        if (current > streak) {
          streak = current;
        }
      } else {
        current = 0;
      }
    });
    res.send({ name: req.params.name, winstreak: streak });
  });
});


router.get('/winrate/:name', function (req, res) {
  var name = req.params.name;
  if (name == "toast") {
    name = "disguised toast";
  }
  var query = BoardState.find({ name: name });
  var wins = 0;
  var losses = 0;
  var draws = 0;
  query.then(function (doc) {
    doc.forEach(function (state) {
      if (state.opponent != "") {
        switch (state.win + 1) {
          case 0:
            losses++;
            break;
          case 1:
            draws++;
            break;
          case 2:
            wins++;
            break;
        }
      }
    });
    res.send({ name: name, wins: wins, losses: losses, draws: draws, winPercentage: wins / (wins + losses + draws) });
  });
});

router.get('/winrate/:name/:opponent', function (req, res) {
  var name = req.params.name;
  if (name == "toast") {
    name = "disguised toast";
  }
  var opponent = req.params.opponent;
  if (name == "toast") {
    name = "disguised toast";
  }
  var query = BoardState.find({ name: name, opponent: opponent });
  var query2 = BoardState.find({ name: opponent, opponent: name });
  var wins = 0;
  var losses = 0;
  var draws = 0;
  query.then(function (doc) {
    query2.then(function (doc2) {
      doc.forEach(function (state) {
        if (!(state.roundNumber <= 3) && !((state.roundNumber - 3) % 5 == 0 && state.roundNumber < 34)) {
          switch (state.win + 1) {
            case 0:
              losses++;
              break;
            case 1:
              draws++;
              break;
            case 2:
              wins++;
              break;
          }
        }
      });
      doc2.forEach(function (state) {
        if (!(state.roundNumber <= 3) && !((state.roundNumber - 3) % 5 == 0 && state.roundNumber < 34)) {
          switch (state.win + 1) {
            case 0:
              losses++;
              break;
            case 1:
              draws++;
              break;
            case 2:
              wins++;
              break;
          }
        }
      });

      res.send({ name: name, opponent: opponent, wins: wins, losses: losses, draws: draws, winPercentage: wins / (wins + losses + draws) });
    });
  });
});


module.exports = router;
