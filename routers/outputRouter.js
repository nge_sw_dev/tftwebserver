var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Game = require('../models/gameSchema.js');
var Round = require('../models/roundSchema.js');
var BoardState = require('../models/boardStateSchema.js');
var ChampionLookup = require('../models/championLookupSchema.js');
var ItemLookup = require('../models/itemLookupSchema.js');

router.get('/', function (req, res) {
  var query = Game.findOne().sort({ date: -1 }).limit(1)
    .populate({
      path: 'rounds',
      model: 'Round',
      select: ['boardState', 'dayNumber', 'gameNumber', 'roundNumber'],
      populate: {
        path: 'boardState',
        select: ['name', 'health', 'roundNumber', 'champions', 'itemNames', 'uncombinedItems', 'activeSynergies', 'championPower', 'itemPower', 'carryChampion', 'top3Champs'],
        model: 'BoardState'
      }
    });
  query.then(function (game) {
    var current = game.currentRound;
    while (current > 1 && game.rounds[current - 1].boardState.length <= 0) {
      current--;
    }
    if (current >= 0) {
      var ret = game.rounds[current - 1];

      var itemPower = [];
      var championPower = [];
      if (ret == null) {
        res.send({ data: ret, itemPower: itemPower, championPower: championPower, live: game.live });
      }
      ret.boardState.sort(itemSort);
      ret.boardState.forEach(function (state) {
        itemPower.push(state.name);
      });
      while (itemPower.length < 8) {
        itemPower.push('');
      }
      ret.boardState.sort(championSort);
      ret.boardState.forEach(function (state) {
        championPower.push(state.name);
      });
      while (championPower.length < 8) {
        championPower.push('');
      }
      res.send({ data: ret, itemPower: itemPower, championPower: championPower, live: game.live });
    } else {
      res.send("current count error!");
    }
  });
});

router.get('/final', function (req, res) {
  var query = Game.findOne({ complete: true }).sort({ date: -1 }).limit(1);
  query.then(function (game) {
    if (!game) {
      res.send({});
    }
    else {
      var stateQuery = BoardState.find({ dayNumber: game.dayNumber, gameNumber: game.gameNumber, health: 0 });
      stateQuery.then(function (stateDoc) {
        stateDoc.push(game.winner);
        res.send(stateDoc);
      });
    }
  });
});

router.get('/test', function (req, res) {
  var query = Game.findOne({ complete: true }).sort({ date: -1 }).limit(1);
  query.then(function (game) {
    var stateQuery = BoardState.find({ dayNumber: game.dayNumber, gameNumber: game.gameNumber }).select(['health', 'name', 'roundNumber']);
    stateQuery.then(function (stateDoc) {
      stateDoc.push(game.winner);
      res.send(stateDoc);
    });
  });

});

var itemSort = function (a, b) {
  var aPower = a.itemPower;
  var bPower = b.itemPower;
  var comparison = 0;
  if (aPower > bPower) {
    comparison = -1;
  } else {
    comparison = 1;
  }
  return comparison;
}

var championSort = function (a, b) {
  var aPower = a.championPower;
  var bPower = b.championPower;
  var comparison = 0;
  if (aPower > bPower) {
    comparison = -1;
  } else {
    comparison = 1;
  }
  return comparison;
}

module.exports = router;
