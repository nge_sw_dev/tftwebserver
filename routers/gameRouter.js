var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Game = require('../models/gameSchema');
var Round = require('../models/roundSchema');
var BoardState = require('../models/boardStateSchema.js');
const { writeLog, clearLog } = require('../js/logHelper');


//Retrieves most recently created game
router.get('/current', function (req, res) {
  var query = Game.findOne().sort({ date: -1 }).limit(1).populate({
    path: 'rounds',
    model: 'Round',
    populate: {
      path: 'boardState',
      model: 'BoardState'
    }
  });
  query.then(function (doc) {
    res.send(doc);
  });
});

//Creates a new game; arguments are day number and game number, and an array of player names in the body
router.get('/new/:day/:game', function (req, res) {
  var game = new Game({
    dayNumber: req.params.day,
    gameNumber: req.params.game,
    currentRound: 1,
    date: Date.now(),
    players: [],
    complete: false,
    winner: {},
    live: 8,
    rounds: []
  });
  //req.body.players.forEach(function(current) {
  //game.players.push(current);
  //});
  var round = new Round({
    dayNumber: req.params.day,
    gameNumber: req.params.game,
    pve: true,
    date: Date.now(),
    roundNumber: 1,
    boardState: [],
    complete: false
  });
  round.save(async function (err) {
    game.rounds.push(round._id);
    game.save(function (err) {
      if (err) throw err;
    });
    console.log("Game " + game.gameNumber + " created.");
    await clearLog();
    writeLog("Game " + game.gameNumber + " created.")
    res.send(game);
  })
});

//Retrieves game object from a specific day and game number
// router.get('/:day/:game', function(req,res){
//   var query = Game.findOne({dayNumber: req.params.day, gameNumber: req.params.game})
//   .populate({
//     path: 'rounds',
//     model: 'Round',
//     populate: {
//       path:'boardState',
//       model: 'BoardState'
//     }
//   });
//   query.then(function(doc) {
//       res.send(doc);
//   });
// });

//Retrieves rou8nd object from a specific day, game, and round number
// router.get('/:day/:game/:round', function(req,res){
//   var query = Round.findOne({dayNumber: req.params.day, gameNumber: req.params.game, roundNumber: req.params.round});
//   query.then(function(doc) {
//     res.send(doc);
//   });
// });


//Retreives all final board states for specified game including winner
router.get('/final/:day/:game', function (req, res) {
  var gameQuery = Game.findOne({ dayNumber: req.params.day, gameNumber: req.params.game });
  var states = [];
  gameQuery.then(function (game) {
    console.log(game);
    states.push(game.winner);
    var boardQuery = BoardState.find({ dayNumber: req.params.day, gameNumber: req.params.game, health: 0 });
    boardQuery.then(function (doc) {
      doc.forEach(function (state, ind) {
        states.push(state);
        if (ind >= doc.length - 1) {
          res.send(states);
        }
      });
    });
  });
});

module.exports = router;
