const fs = require('fs');
const lineByLine = require('n-readlines');

var getLog = async function (n) {
    var ret = [];
    var liner = new lineByLine('./log.txt');
    let line;
    let count = 0;
    while (line = liner.next()) {
        var text = line.toString().replace("\r", "")
        ret.push({
            'id': count + 1,
            'message': text
        });
        count++;
        if (count >= n && liner.fd != null) {
            liner.close();
        }
    }
    return ret;
}

var writeLog = async function (line) {
    line = line.replace("\r\n", "").replace("\r", "").replace("\n", "");
    var text = fs.readFileSync('./log.txt', 'utf8');
    fs.writeFileSync("./log.txt", line + '\n' + text);
}

var clearLog = async function () {
    fs.writeFileSync('./log.txt', '');
}
// async function main() {
//     await clearLog();
//     await writeLog("1");
//     await writeLog("2");
//     await writeLog("3");
//     await writeLog("4");
//     var output = await getLog(100);
//     console.log(output);
// }
// main();

module.exports = {
    getLog,
    writeLog,
    clearLog

}