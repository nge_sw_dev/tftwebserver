var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Round = require('./roundSchema.js');
var BoardState = require('./boardStateSchema.js');

var gameSchema = new Schema({
  dayNumber: Number,
  gameNumber: Number,
  currentRound: Number,
  date: Date,
  live: Number,
  players: [String],
  standings: [String],
  winner: Object,
  complete: Boolean,
  rounds: [{type: Schema.Types.ObjectId, ref: Round}]
});

var calculateWinner = function(game) {
  return new Promise (resolve => {
    var query = Round.findOne({gameNumber: game.gameNumber, roundNumber: game.currentRound + 1}).populate('boardState').lean();
    query.then(function(doc){
      doc.boardState.forEach(function (state) {
        if(state.health > 0) {
          resolve(state);
        }
      });
    });
  });
}

var calculatePlayers = function(game) {
  return new Promise(resolve => {
    var query = Round.findOne({dayNumber: game.dayNumber, gameNumber: game.gameNumber, roundNumber: 1}).populate('boardState').lean();
    var names = [];
    query.then(function(doc) {
      doc.boardState.forEach(function(state) {
        names.push(state.name);
      });
      resolve(names);
    });
  });
}

gameSchema.method('calculateWinner', calculateWinner);
gameSchema.method('calculatePlayers', calculatePlayers);


var Game = mongoose.model('Game', gameSchema);
module.exports = Game;
