var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * @swagger
 *  components:
 *    schemas:
 *      ChampionLookupSchema:
 *        type: object
 *        required:
 *          - name
 *          - origin
 *          - class
 *          - originWeight
 *          - classWeight
 *          - cost
 *        properties:
 *          name:
 *            type: string
 *          origin:
 *            type: Array
 *            items:
 *              type: string
 *          class:
 *            type: Array
 *            items:
 *              type: string 
 *          originWeight:
 *            type: number
 *          classWeight:
 *            type: number
 *          cost:
 *            type: number
 */
var ChampionLookupSchema = new Schema({
  name: String,
  origin: [String],
  class: [String],
  originWeight: Number,
  classWeight: Number,
  cost: Number
});

var ChampionLookup = mongoose.model('ChampionLookup', ChampionLookupSchema);
module.exports = ChampionLookup;
