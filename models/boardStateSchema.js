var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ChampionLookup = require('./championLookupSchema.js');
var ItemLookup = require('../models/itemLookupSchema.js');
var Hashtable = require('simple-hashtable');
var Hashset = require('hashset');

var boardStateSchema = new Schema({
	name: String,
	spotter: String,
	health: Number,
	opponent: String,
	win: Number,
	dayNumber: Number,
	gameNumber: Number,
	roundNumber: Number,
	pve: Boolean,
	champions: [Object],
	items: [Object],
	uncombinedItems: [String],
	synergies: Object,
	activeSynergies: [Object],
	championPower: Number,
	itemPower: Number,
	carryChampion: Object,
	submitted: Boolean,
	championNames: [String],
	itemNames: [String],
	championDoc: Object,
	itemDoc: Object,
	top3Champs: [String]
});


var championPower = function () {
	return new Promise(resolve => {
		var powerMatrix = [[1, 1, 2, 2, 3, 0, 4], [2, 2, 3, 4, 5, 0, 6], [3, 4, 5, 6, 8, 0, 10]];
		var power = 0;
		var champNames = this.championNames;
		var champions = this.champions;
		var doc = this.championDoc;
		doc.forEach(function (championLookup) {
			champions.forEach(function (champion) {
				if (champion.name == championLookup.name) {
					power += powerMatrix[champion.level - 1][championLookup.cost - 1];
				}
			});
		});
		resolve(power);
	});
};

//method to calculate champion power of a board state
var carryCalculate = function () {
	return new Promise(resolve => {
		var powerMatrix = [[1, 1, 2, 2, 3, 0, 4], [2, 2, 3, 4, 5, 0, 6], [3, 4, 5, 6, 8, 0, 10]];
		var itemNames = this.itemNames;
		var champNames = this.championNames;
		var champions = this.champions;
		var itemDoc = this.itemDoc;
		var championDoc = this.championDoc;
		var carry = {
			champion: {},
			power: 0
		};
		champions.forEach(function (champion) {
			var tempPower = 0;
			championDoc.forEach(function (docChampion) {
				if (champion.name == docChampion.name) {
					tempPower += powerMatrix[champion.level - 1][docChampion.cost - 1];
					champion.items.forEach(function (item) {
						itemDoc.forEach(function (itemDoc) {
							if (item == itemDoc.name) {
								tempPower += itemDoc.itemPower;
							}
						});
					});
				}
				if (tempPower > carry.power) {
					carry.champion = champion;
					carry.power = tempPower;
				}
			});
		});
		resolve(carry.champion);
	});
};

//method to calculate top 3 most valuable units
var top3ChampionCalculate = function () {
	return new Promise(resolve => {
		var powerMatrix = [[1, 1, 2, 2, 3, 0, 4], [2, 2, 3, 4, 5, 0, 6], [3, 4, 5, 6, 8, 0, 10]];
		var itemNames = this.itemNames;
		var champNames = this.championNames;
		var champions = this.champions;
		var itemDoc = this.itemDoc;
		var championDoc = this.championDoc;
		var output = [];
		var ret = [];

		champions.forEach(function (champion) {
			var tempPower = 0;
			championDoc.forEach(function (docChampion) {
				if (champion.name == docChampion.name) {
					tempPower += powerMatrix[champion.level - 1][docChampion.cost - 1];
					champion.items.forEach(function (item) {
						itemDoc.forEach(function (itemDoc) {
							if (item == itemDoc.name) {
								tempPower += itemDoc.itemPower;
							}
						});
					});
				}
			});
			output.push({
				"name": champion.name,
				"power": tempPower
			});
		});
		output.sort(function (a, b) {
			return b.power - a.power;
		});
		var max;
		if (output.length < 3) {
			max = output.length;
		} else {
			max = 3;
		}
		for (var i = 0; i < max; i++) {
			ret[i] = output[i].name;
		}

		resolve(ret);
	});
};

//method to calculate item power of a board BoardState
var calculateItemPower = function () {
	return new Promise(resolve => {
		var itemNames = this.itemNames;
		var totalPower = 0;
		var doc = this.itemDoc;
		doc.forEach(function (item) {
			itemNames.forEach(function (itemName) {
				if (itemName == item.name) {
					totalPower += item.itemPower;
				}
			});
		});
		resolve(totalPower);
	});
}


//calculate the current synergies that exist in the board state
var calculateSynergies = function () {
	return new Promise(resolve => {
		var table = new Hashtable();
		var data = ["Daredevil", "Fabled", "The Boss", "Blacksmith", "Emperor", "Exile", "Fortune", "Ninja", "Spirit", "Slayer", "Syphoner", "Cultist", "Dragonsoul", "Elderwood", "Enlightened", "Warlord", "Adept", "Assassin", "Executioner", "Keeper", "Mage", "Mystic", "Sharpshooter", "Divine", "Brawler", "Duelist", "Vanguard"];
		data.forEach(function (word) {
			table.put(word, 0);
		});
		var championNames = this.championNames;
		var champions = this.champions;
		var itemNames = this.itemNames;
		var champDoc = this.championDoc;
		var itemDoc = this.itemDoc;
		var seen = new Hashset();
		champions.forEach(function (champion) {
			//if we havent seen the champion count the synergies
			if (!seen.contains(champion.name)) {
				champDoc.forEach(function (championDoc) {
					//if we find the doc put the origin and class into the tam
					if (champion.name == championDoc.name) {
						championDoc.origin.forEach(function (origin) {
							table.put(origin, table.get(origin) + championDoc.originWeight);
						});
						championDoc.class.forEach(function (class1) {
							table.put(class1, table.get(class1) + championDoc.classWeight);
						});
					}
				});
				//add the name to our seen table
				seen.add(champion.name);
			}
			//count the items for this champion
			champion.items.forEach(function (item) {
				itemDoc.forEach(function (itemDoc) {
					if (item == itemDoc.name) {
						itemDoc.origin.forEach(function (origin) {
							table.put(origin, table.get(origin) + 1);
						});
						itemDoc.class.forEach(function (class1) {
							table.put(class1, table.get(class1) + 1);
						});
					}
				});
			});
		});
		var synergies = {};
		data.forEach(function (word) {
			var val = table.get(word);
			if (val > 0) {
				synergies[word] = val;
			}
		});
		var output = {
			synergies: synergies,
			activeSynergies: activeSynergies(table)
		}
		resolve(output);

	});
}


//TODO: update for Set 3/4 Synergies
var activeSynergies = function (table) {
	var actives = [];
	//1 levels
	if (table.get("Fabled") >= 3) {
		actives.push({ name: "Fabled", value: 3, rank: "Gold" });
	}
	if (table.get("Daredevil") >= 1) {
		actives.push({ name: "Daredevil", value: 1, rank: "Gold" });
	}
	if (table.get("The Boss") >= 1) {
		actives.push({ name: "The Boss", value: 1, rank: "Gold" });
	}
	if (table.get("Blacksmith") >= 1) {
		actives.push({ name: "Blacksmith", value: 1, rank: "Gold" });
	}
	if (table.get("Emperor") >= 1) {
		actives.push({ name: "Emperor", value: 1, rank: "Gold" });
	}

	//2 levels
	if (table.get("Exile") >= 1) {
		if (table.get("Exile") >= 2) {
			actives.push({ name: "Exile", value: 2, rank: "Gold" });
		} else {
			actives.push({ name: "Exile", value: 1, rank: "Bronze" })
		}
	}
	if (table.get("Fortune") >= 3) {
		if (table.get("Fortune") >= 6) {
			actives.push({ name: "Fortune", value: 6, rank: "Gold" });
		} else {
			actives.push({ name: "Fortune", value: 3, rank: "Bronze" })
		}
	}
	if (table.get("Spirit") >= 2) {
		if (table.get("Spirit") >= 4) {
			actives.push({ name: "Spirit", value: 4, rank: "Gold" });
		} else {
			actives.push({ name: "Spirit", value: 2, rank: "Bronze" })
		}
	}
	if (table.get("Slayer") >= 3) {
		if (table.get("Slayer") >= 6) {
			actives.push({ name: "Slayer", value: 6, rank: "Gold" });
		} else {
			actives.push({ name: "Slayer", value: 3, rank: "Bronze" })
		}
	}
	if (table.get("Syphoner") >= 2) {
		if (table.get("Syphoner") >= 4) {
			actives.push({ name: "Syphoner", value: 4, rank: "Gold" });
		} else {
			actives.push({ name: "Syphoner", value: 2, rank: "Bronze" })
		}
	}

	//2 levels exact
	if (table.get("Ninja") == 1) {
		actives.push({ name: "Ninja", value: 1, rank: "Bronze" });
	}
	if (table.get("Ninja") == 4) {
		actives.push({ name: "Ninja", value: 4, rank: "Gold" });
	}

	//3 levels
	if (table.get("Cultist") >= 3) {
		if (table.get("Cultist") >= 6) {
			if (table.get("Cultist") >= 9) {
				actives.push({ name: "Cultist", value: 9, rank: "Gold" });
			} else {
				actives.push({ name: "Cultist", value: 6, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Cultist", value: 3, rank: "Bronze" });
		}
	}
	if (table.get("Dragonsoul") >= 3) {
		if (table.get("Dragonsoul") >= 6) {
			if (table.get("Dragonsoul") >= 9) {
				actives.push({ name: "Dragonsoul", value: 9, rank: "Gold" });
			} else {
				actives.push({ name: "Dragonsoul", value: 6, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Dragonsoul", value: 3, rank: "Bronze" });
		}
	}
	if (table.get("Elderwood") >= 3) {
		if (table.get("Elderwood") >= 6) {
			if (table.get("Elderwood") >= 9) {
				actives.push({ name: "Elderwood", value: 9, rank: "Gold" });
			} else {
				actives.push({ name: "Elderwood", value: 6, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Elderwood", value: 3, rank: "Bronze" });
		}
	}
	if (table.get("Enlightened") >= 2) {
		if (table.get("Enlightened") >= 4) {
			if (table.get("Enlightened") >= 6) {
				actives.push({ name: "Enlightened", value: 6, rank: "Gold" });
			} else {
				actives.push({ name: "Enlightened", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Enlightened", value: 2, rank: "Bronze" });
		}
	}
	if (table.get("Warlord") >= 3) {
		if (table.get("Warlord") >= 6) {
			if (table.get("Warlord") >= 9) {
				actives.push({ name: "Warlord", value: 9, rank: "Gold" });
			} else {
				actives.push({ name: "Warlord", value: 6, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Warlord", value: 3, rank: "Bronze" });
		}
	}
	if (table.get("Assassin") >= 2) {
		if (table.get("Assassin") >= 4) {
			if (table.get("Assassin") >= 6) {
				actives.push({ name: "Assassin", value: 6, rank: "Gold" });
			} else {
				actives.push({ name: "Assassin", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Assassin", value: 2, rank: "Bronze" });
		}
	}
	if (table.get("Keeper") >= 2) {
		if (table.get("Keeper") >= 4) {
			if (table.get("Keeper") >= 6) {
				actives.push({ name: "Keeper", value: 6, rank: "Gold" });
			} else {
				actives.push({ name: "Keeper", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Keeper", value: 2, rank: "Bronze" });
		}
	}
	if (table.get("Mage") >= 3) {
		if (table.get("Mage") >= 5) {
			if (table.get("Mage") >= 7) {
				actives.push({ name: "Mage", value: 7, rank: "Gold" });
			} else {
				actives.push({ name: "Mage", value: 5, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Mage", value: 3, rank: "Bronze" });
		}
	}
	if (table.get("Mystic") >= 2) {
		if (table.get("Mystic") >= 4) {
			if (table.get("Mystic") >= 6) {
				actives.push({ name: "Mystic", value: 6, rank: "Gold" });
			} else {
				actives.push({ name: "Mystic", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Mystic", value: 2, rank: "Bronze" });
		}
	}
	if (table.get("Sharpshooter") >= 2) {
		if (table.get("Sharpshooter") >= 4) {
			if (table.get("Sharpshooter") >= 6) {
				actives.push({ name: "Sharpshooter", value: 6, rank: "Gold" });
			} else {
				actives.push({ name: "Sharpshooter", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Sharpshooter", value: 2, rank: "Bronze" });
		}
	}

	//3 levels consecutive
	if (table.get("Adept") >= 2) {
		if (table.get("Adept") >= 4) {
			actives.push({ name: "Adept", value: 4, rank: "Gold" });
		} else {
			if (table.get("Adept") >= 3) {
				actives.push({ name: "Adept", value: 3, rank: "Silver" });
			} else {
				actives.push({ name: "Adept", value: 2, rank: "Bronze" });
			}
		}
	}
	if (table.get("Executioner") >= 2) {
		if (table.get("Executioner") >= 4) {
			actives.push({ name: "Executioner", value: 4, rank: "Gold" });
		} else {
			if (table.get("Executioner") >= 3) {
				actives.push({ name: "Executioner", value: 3, rank: "Silver" });
			} else {
				actives.push({ name: "Executioner", value: 2, rank: "Bronze" });
			}
		}
	}

	//4 Levels
	if (table.get("Divine") >= 2) {
		if (table.get("Divine") >= 4) {
			if (table.get("Divine") >= 6) {
				if (table.get("Divine") >= 8) {
					actives.push({ name: "Divine", value: 8, rank: "Diamond" });
				} else {
					actives.push({ name: "Divine", value: 6, rank: "Gold" });
				}
			} else {
				actives.push({ name: "Divine", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Divine", value: 2, rank: "Bronze" });
		}
	}
	if (table.get("Brawler") >= 2) {
		if (table.get("Brawler") >= 4) {
			if (table.get("Brawler") >= 6) {
				if (table.get("Brawler") >= 8) {
					actives.push({ name: "Brawler", value: 8, rank: "Diamond" });
				} else {
					actives.push({ name: "Brawler", value: 6, rank: "Gold" });
				}
			} else {
				actives.push({ name: "Brawler", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Brawler", value: 2, rank: "Bronze" });
		}
	}
	if (table.get("Duelist") >= 2) {
		if (table.get("Duelist") >= 4) {
			if (table.get("Duelist") >= 6) {
				if (table.get("Duelist") >= 8) {
					actives.push({ name: "Duelist", value: 8, rank: "Diamond" });
				} else {
					actives.push({ name: "Duelist", value: 6, rank: "Gold" });
				}
			} else {
				actives.push({ name: "Duelist", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Duelist", value: 2, rank: "Bronze" });
		}
	}
	if (table.get("Vanguard") >= 2) {
		if (table.get("Vanguard") >= 4) {
			if (table.get("Vanguard") >= 6) {
				if (table.get("Vanguard") >= 8) {
					actives.push({ name: "Vanguard", value: 8, rank: "Diamond" });
				} else {
					actives.push({ name: "Vanguard", value: 6, rank: "Gold" });
				}
			} else {
				actives.push({ name: "Vanguard", value: 4, rank: "Silver" });
			}
		} else {
			actives.push({ name: "Vanguard", value: 2, rank: "Bronze" });
		}
	}

	return (actives);
}

boardStateSchema.method('calculateChampionPower', championPower);
boardStateSchema.method('calculateCarry', carryCalculate);
boardStateSchema.method('calculateItemPower', calculateItemPower);
boardStateSchema.method('calculateSynergies', calculateSynergies);
boardStateSchema.method('top3ChampionCalculate', top3ChampionCalculate);



var BoardState = mongoose.model('BoardState', boardStateSchema);
module.exports = BoardState;
