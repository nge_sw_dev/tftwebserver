var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemLookupSchema = new Schema({
  name: String,
  combined: Boolean,
  recipe: [String],
  itemPower: Number,
  origin: [String],
  class: [String]
});

var ItemLookup = mongoose.model('ItemLookup', ItemLookupSchema);
module.exports = ItemLookup;
