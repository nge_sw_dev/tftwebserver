var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var BoardState = require('./boardStateSchema.js');


var roundSchema = new Schema({
  dayNumber: Number,
  gameNumber: Number,
  roundNumber: Number,
  pve: Boolean,
  date: Date,
  boardState: [{type: Schema.Types.ObjectId, ref: 'BoardState'}],
  complete: Boolean
});


var Round = mongoose.model('Round', roundSchema);
module.exports = Round;
