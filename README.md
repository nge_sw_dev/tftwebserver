Deployment: 
1. Download directory
2. run 'npm install'
3. Create empty 'log.txt' file
4. Start the index.js file. I use forever with the command 'forever start index.js;


Changes for new sets:
Update the mongoDB connect string in index.js line 39
Update 'calculateSynergies' method in boardStateSchema.js
Add champion and item lookups to the database by sending entries to /items/lookup and /champions/lookup respectively
